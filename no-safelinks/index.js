// ==UserScript==
// @name        Un-safe Safelinks
// @namespace   http://hugo.hornquist.se
// @version     0.1
// @author      hugo@lysator.liu.se
// @match       *://*/*
// @description Inputs the current date into the current text field.
// @updateURL   https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/no-safelinks/index.js
// @downloadURL https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/no-safelinks/index.js
// @source      https://git.lysator.liu.se/hugo/web-monkey-scripts/-/tree/master/no-safelinks
// ==/UserScript==

(function() {
	'use strict';
	for (let el of document.getElementsByTagName('a')) {
		let url = new URL(el.href);
		if (url.href === 'https://eur01.safelinks.protection.outlook.com') {
			a.href = url.searchParams.get('url')
		}
	}
})();
