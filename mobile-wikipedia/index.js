// ==UserScript==
// @name        Go To mobile wikipedia
// @namespace   http://hugo.hornquist.se
// @match       https://*.wikipedia.org/wiki/*
// @grant       none
// @version     1.0
// @author      hugo@lysator.liu.se
// @description Go To mobile wikipedia
// @source       https://git.lysator.liu.se/hugo/web-monkey-scripts/-/tree/master/mobile-wikipedia
// @updateURL    https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/modile-wikipedia/index.js
// @downloadURL  https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/mobile-wikipedia/index.js
// ==/UserScript==

(function () {
	'use strict';

	const translations = {
		'sv': {
			'Go to mobile site': 'Gå till mobilsida',
			'Mobile': 'Mobil',
			'Alt': 'Alt',
			'Shift': 'Skift',
		}
	}

	function translate(to, src) {
		let dict = translations[to];
		if ((dict = translations[to])) {
			return dict[src] || src
		} else {
			return src
		}
	}

	const nav_btn = 'm'

	let pageLang = document.documentElement.lang;
	let browserLang_ = navigator.language.split('-');
	let browserLang
	let browserRegion = null
	if (browserLang_.length == 2) {
		browserLang = browserLang_[0]
		browserRegion = browserLang_[1]
	} else {
		browserLang = browserLang_.join('-')
	}

	let li = document.createElement('li');
	li.id = 'ca-nstab-mobile'
	li.classList.add('mw-list-item')

	let a = document.createElement('a');
	a.href = document.querySelector('#footer-places-mobileview a').href
	a.title = `${translate(pageLang, 'Go to mobile site')} [${translate(browserLang, 'Alt')}+${translate(browserLang, 'Shift')}+${nav_btn}]`
	a.accesskey = nav_btn

	let span = document.createElement('span');
	span.textContent = translate(pageLang, 'Mobile')

	a.appendChild(span)
	li.appendChild(a)

	let ul = document.querySelector('#p-namespaces .vector-menu-content-list')
	ul.appendChild(li)
})();
