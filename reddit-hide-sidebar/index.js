// ==UserScript==
// @name         old.reddit hide sidebar
// @namespace    http://hugo.hornquist.se
// @version      1.2.1
// @description  Adds toggle to hide Reddit sidebar
// @author       hugo@lysator.liu.se
// @match        https://*.reddit.com/*
// @match        https://old.reddit.com/*
// @match        https://www.reddit.com/r/*/wiki/*
// @match        https://np.reddit.com/*
// @source       https://git.lysator.liu.se/hugo/web-monkey-scripts/-/tree/master/reddit-hide-sidebar
// @updateURL    https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/reddit-hide-sidebar/index.js
// @downloadURL  https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/reddit-hide-sidebar/index.js
// ==/UserScript==

(function() {
	'use strict';

	let btn = document.createElement('button');
	let div = document.createElement('div');
	div.style = `
	position: absolute;
	z-index: 100;
	left: -2em;
	width: 2em;
	height: 2em;
	display: grid;
	top: 0;
	padding: 0;
	`

	div.appendChild(btn);

	let sidebar = document.getElementsByClassName('side')[0];
	sidebar.appendChild(div);

	let svgns = 'http://www.w3.org/2000/svg'
	let svg = document.createElementNS(svgns, 'svg');
	svg.id = 'triangle-icon'
	let path = document.createElementNS(svgns, 'path');
	svg.setAttribute('viewBox', '0 0 100 100');
	path.setAttribute('d', `M0 0L100 50L0 100`)

	svg.appendChild(path)
	btn.appendChild(svg)

	btn.onclick = () => {
		sidebar.classList.toggle('hidden');
		let isHidden = sidebar.classList.contains('hidden')
		let i = 0;
		/* Will run 50 times */
		let intervalID = window.setInterval(() => {
			let x = isHidden ? 2 * i : 100 - 2 * i;
			path.setAttribute('d', `M${x} 0L${100 - x} 50L${x} 100`);
			i += 1;
		}, 10);
		window.setTimeout(() => {
			window.clearInterval(intervalID)
			let x = isHidden ? 100 : 0;
			path.setAttribute('d', `M${x} 0L${100 - x} 50L${x} 100`);
		}, 500)
	}

	let style = document.createElement('style');

	style.innerText = `
.side {
	transition: 0.5s;
		position: relative;
}
.side.hidden {
	 width: 0px;
}
#triangle-icon {
	fill: black;
}
.res-nightmode #triangle-icon {
	fill: white;
}
`;

	document.getElementsByTagName('head')[0].appendChild(style);
})();

