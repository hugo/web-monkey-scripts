// ==UserScript==
// @name        Date Button
// @namespace   http://hugo.hornquist.se
// @version     0.1.7
// @author      hugo@lysator.liu.se
// @match       *://*/*
// @description Inputs the current date into the current text field.
// @updateURL   https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/date-button/index.js
// @downloadURL https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/date-button/index.js
// @source      https://git.lysator.liu.se/hugo/web-monkey-scripts/-/tree/master/date-button
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// ==/UserScript==

(function () {
    'use strict';

    let btn_id = 'id-37582c25-78ad-4922-a19b-244361f42ec0'

    function serialize_event(e) {
        return {
            'key':   e.key,
            'ctrl':  e.ctrlKey,
            'alt':   e.altKey,
            'shift': e.shiftKey,
        }
    }

    function deserialize_event(data) {
        return new KeyboardEvent('keydown', {
            ctrlKey:  data.ctrl,
            altKey:   data.alt,
            shiftKey: data.shift,
            key:      data.key,
        })
    }

    function event_matches(a, b) {
        return a.ctrlKey  == b.ctrlKey  &&
               a.altKey   == b.altKey   &&
               a.shiftKey == b.shiftKey &&
               a.key      == b.key;
    }

    function formatKeyEvent(e) {
        let s = '';
        if (e.ctrlKey)  s += 'Ctrl-';
        if (e.altKey)   s += 'Alt-';
        if (e.shiftKey) s += 'Shift-';
        if (e.key != 'Shift' && e.key != 'Control' && e.key != 'Alt')  s += e.key;
        return s;
    }

    let btn = document.createElement('button');
    btn.id = btn_id;

    let registered_key_combo = GM_getValue('date-key');
    console.log(registered_key_combo);

    if (registered_key_combo == null) {
        btn.textContent = 'Register keycode'
    } else {
        registered_key_combo = deserialize_event(registered_key_combo)
        btn.textContent = formatKeyEvent(registered_key_combo);
    }
    console.log(registered_key_combo);

    btn.addEventListener('click', (e) => {
        btn.textContent = 'Input key sequence ...'
    })

    document.body.appendChild(btn);

    let state = false
    btn.addEventListener('keydown', (e) => {
        console.log(e);
        let s = formatKeyEvent(e)
        console.log(s);
        btn.textContent = s
        state = 'down'
    })

    btn.addEventListener('keyup', (e) => {
        if (state != 'down') return;
        state = 'up'
        registered_key_combo = e;
        GM_setValue('date-key', serialize_event(e))
        btn.blur();
        e.preventDefault();
    })

    document.addEventListener('keydown', (e) => {
        if (event_matches(e, registered_key_combo)) {
            if (! document.activeElement) return;
            let start = document.activeElement.selectionStart;
            let end   = document.activeElement.selectionEnd;
            if (start == undefined || end == undefined) return;
            let s = document.activeElement.value
            s = s.substring(0, start) + (new Date).toISOString() + s.substring(end)
            document.activeElement.value = s;
            e.preventDefault();
        }
    });

    let style = document.createElement('style');
    style.textContent = `
    #${btn_id} {
        position: fixed;
        bottom: 0;
        right: 1em;
        z-index: 10000;
        background: lightgray;
        border: 2px solid black;
    }
    #${btn_id}:focus {
        background: pink;
    }
    `

    document.head.appendChild(style);

})();
