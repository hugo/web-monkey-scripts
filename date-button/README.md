Date Inserter
=============
Simple scripts which inserts the current date-time when a given button
is pressed.

When installed a button will appear in the top left corner, pressing
it prompts you to enter a key sequence. Later, whenever that key
sequence is entered the current date-time is entered into the current
text field.
