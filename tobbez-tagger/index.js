// ==UserScript==
// @name         Tobbez fix
// @namespace    http://hugo.hornquist.se/
// @version      1.1.1
// @description  Tagging interface for a site
// @author       hugo@lysator.liu.se
// @match        https://wrya.net/services/wallpaper/scrape4/*
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @updateURL    https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/tobbez-tagger/index.js
// @downloadURL  https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/tobbez-tagger/index.js
// @require      http://lysator.liu.se/~hugo/js/jquery-3.3.1.min.js
// @require      http://lysator.liu.se/~hugo/js/jquery-ui-1.12.1.min.js
// ==/UserScript==

var taglist = [];
let default_taglist = ["desktop", "anime", "game"];

function tag_it() {
    let tag = this.innerText;
    document.location.href='/services/wallpaper/tag.php?t=' + tag + '&i='+document.location.pathname.substr(20);
}

function new_tag() {
    let tag = window.prompt("Mata in tag:", "");
    taglist.push(tag);
    GM_setValue("tags", taglist);
    document.location.href='/services/wallpaper/tag.php?t=' + tag + '&i='+document.location.pathname.substr(20);
}

(function() {
    //'use strict';

    taglist = GM_getValue("tags");
    if (taglist === undefined || taglist.length == 0) {
        taglist = default_taglist;
        GM_setValue("tags", taglist);
    }

    let ctrl = $("<div id='ctrl'></div>");
    for (let tag of taglist) {
        var btn = $("<button>" + tag + "</button>");
        btn.click(tag_it);
        ctrl.append(btn);
    }

    let newtag = $("<button><b>[Custom]</b></button>");
    newtag.click(new_tag);
    ctrl.append(newtag);

    let skiptag = $("<button><b>[Skip]</b></button>");
    skiptag.click(function () {
        document.location.href='/services/wallpaper/4chan.php';
    });
    ctrl.append(skiptag);

    ctrl.draggable({
        stop: function (event, ui) {
            GM_setValue("top", ui.position.top);
            GM_setValue("left", ui.position.left);
        },
        snap: "img"
    });

    ctrl.css("top", GM_getValue("top", 0));
    ctrl.css("left", GM_getValue("left", 0));

    GM_addStyle ( `
#ctrl {
    position: relative;
    opacity: 0.3;
    background: white;
    border: 1px solid black;
    border-radius: 1em;
    padding: 1em;
    max-width: 15em;
}
#ctrl:hover {
    opacity: 1;
}
#ctrl > button {
    display: block;
    width: 100%;
    margin-top: 0.4em;
    margin-bottom: 0.4em;
    height: 1.5em;
}
#ctrl button:first-of-type {
    margin-top: 0;
}
#ctrl button:last-of-type {
    margin-bottom: 0;
}
` );

    $("body").append(ctrl);
})();
