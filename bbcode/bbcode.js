/* Element containing the BB-code */
let el = document.getElementsByClassName("nfo")[0].children[0]
let str = el.innerText

var r = /\[(\/)?(\w{0,5})(=([^\]]*))?\]/g

let stk = [];

let result_str = "";

/* Index of first unhandled character in input string
 * Should guarantee that nothing gets parsed twice */
let last_idx = 0;

let result;
while ((result = r.exec(str)) !== null) {
    index = result.index
    var [matched, end, tag, _, param] = result
    if (! end) { /* if start tag */
        o = { tag: tag
			, str: ""
			, index: index + matched.length
			, param: param
			}
        if (stk.length === 0) {
            result_str += str.substring(last_idx, index);
        } else {
            var other = stk.last();
            other.str += str.substring(other.index, index);
        }
        last_idx = o.index;
        stk.push(o)

    } else { /* if end tag */
        parent = stk.pop()
        if (parent.tag !== tag) {
            alert("Non matching tags");
        } else {
            parent.str += str.substring(last_idx, index);
            parent.str = parent.str.strip()
            last_idx = index + matched.length;
            var s;
            switch (parent.tag) {
            case "url":
				s = "<a href='"
                if (typeof parent.param === "undefined") {
                    s += parent.str;
                } else {
                    s += parent.param;
                }
                s += "'>" + parent.str + "</a>";
                break;

            case "img":
                s = "<img src='" + parent.str + "'/>"
                break;

            default:
                s = "<b>Unknown tag <pre>" + parent.str + "</pre></b>"
                break;
            }
            if (stk.length === 0) {
                result_str += s;
            } else {
                stk.last().str += s;
            }
        }
    }
}

el.innerHTML = result_str
