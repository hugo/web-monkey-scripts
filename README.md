Web Monkey Scripts
==================
A collection of user scripts for different browsers. Mainly tested in chrome.

Easiest way to import them is to use their raw URL's from this repo.
For example; `reddit-hide-sidebar` has its main/index file @
<https://git.lysator.liu.se/hugo/web-monkey-scripts/blob/master/reddit-hide-sidebar/index.js>

Simply replacing blob => raw gives the actual text file, fit for a direct
import.
<https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/reddit-hide-sidebar/index.js>

