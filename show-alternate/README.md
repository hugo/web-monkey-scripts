Show Alternate
==============

Add all `link[rel=alternate]` elements in document to body.
