// ==UserScript==
// @name        Show alternate links
// @namespace   http://hugo.hornquist.se
// @version     0.2.1
// @description Show alternate links
// @author      hugo@lysator.liu.se
// @match       *://*/*
// @updateURL   https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/show-alternate/index.js
// @downloadURL https://git.lysator.liu.se/hugo/web-monkey-scripts/raw/master/show-alternate/index.js
// @source      https://git.lysator.liu.se/hugo/web-monkey-scripts/-/tree/master/show-alternate
// @grant       GM_addStyle
// ==/UserScript==

(function () {
	'use strict';

	let id = `id-${Math.floor(Math.random() * 1e6)}`

	let outer = document.createElement('div');
	let inner = document.createElement('div');
	outer.id = id
	let ul = document.createElement('ul');
	outer.appendChild(inner);
	inner.appendChild(ul);

	let link_count = 0
	for (let link of document.querySelectorAll('link[rel=alternate]')) {
		link_count++
		let li = document.createElement('li');
		let a = document.createElement('a')
		a.href = link.href
		a.textContent = `${link.type} - ${link.title}`
		li.appendChild(a)
		ul.appendChild(li)
	}

	if (link_count > 0) {
		document.body.appendChild(outer)
	}

	GM_addStyle(`
#${id} {
	margin: 0 !important;
	position: absolute;
	top: 0;
	right: 0;
	display: block;
	width: 2em;
	min-height: 2em;
	background: rgba(255,0,0,0.5);
	color: white;
	font-weight: bold;
	padding: 1ex;
}

#${id}:hover {
	width: unset;
	z-index: 10000000;
}

#${id} ul {
	padding: revert;
}

#${id} > div {
	display: none;
}

#${id}:hover > div {
	display: block;
}
`)
})();
